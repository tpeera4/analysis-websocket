FROM gradle:4.5-jdk8
USER root

WORKDIR /app

# Adding source, compile and package into a fat jar
ADD ./server /app
RUN ["gradle", "wrapper", "--gradle-version", "4.0"]

#RUN ["gradle", "resolveDependencies"]

#RUN ["./gradlew", "shadowJar"]
RUN ["./gradlew","build"]

EXPOSE 8080

CMD ["java", "-jar", "build/libs/gs-messaging-stomp-websocket-0.1.0.jar"]

#CMD ["/usr/lib/jvm/java-8-openjdk-amd64/bin/java", "-jar", "target/sparkexample-jar-with-dependencies.jar"]
