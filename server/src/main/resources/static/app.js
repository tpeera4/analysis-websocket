var stompClient = null;

function connect() {
   	/*
   	 * new SockJS(endpoint_url) e.g. localhost:8080/gs-guide-websocket
   	 * relative here if this webpage is running from the same host
   	 */
     var socket = new SockJS('/service-endpoint');
     stompClient = Stomp.over(socket);
//	var currentHost = location.href.replace(/https?:\/\//i, "");
//    stompClient = Stomp.client('ws://'+currentHost+'service-endpoint/websocket');
//	stompClient = Stomp.client('ws://localhost:8080/service-endpoint/websocket');
    stompClient.connect({}, function (frame) {
    	console.log('Connected to the refactoring endpoint');
        
        /*
         * follow convention of /user/queue/<message-destination>
         * <message-destination> corresponds to @MessageMapping argument in the controller class
         */
        stompClient.subscribe('/user/queue/request', function (msg) {
        	console.log('received a message from the server');
            console.log(JSON.parse(msg.body));
        });
        
    });
}

function disconnect() {
	
    if (stompClient !== null) {
        stompClient.disconnect();
    }
    console.log("Disconnected");
}

function sendName() {
	stompClient.send("/app/request", {}, JSON.stringify({type: 'test' }));
}


$(function () {
    $("form").on('submit', function (e) {
        e.preventDefault();
    });
    $( "#connect" ).click(function() { connect(); });
    $( "#disconnect" ).click(function() { disconnect(); });
    $( "#send" ).click(function() { sendName(); });
});
