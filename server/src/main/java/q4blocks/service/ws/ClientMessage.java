package q4blocks.service.ws;

public class ClientMessage {

    private String type;
    private String body;
    

	public ClientMessage() {
    }

	public ClientMessage(String type) {
        this.type = type;
    }

    public String getBody() {
		return body;
	}


	public String getType() {
        return type;
    }

    public void setBody(String body) {
		this.body = body;
	}



    public void setType(String type) {
        this.type = type;
    }
}
