package q4blocks.service.ws;


import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.simp.annotation.SendToUser;
import org.springframework.stereotype.Controller;


import main.RefactoringManager;



@Controller
public class RefactoringController {

    @MessageMapping("/request") // message destination
    @SendToUser // for peer-to-peer communication
    public ServerMessage clientRequestHandler(ClientMessage clientMsg) throws Exception {
    	long start_time = System.nanoTime();
    	
    	
    	
    	// test connection
    	if(clientMsg.getType().equals("test")) {
    		ServerMessage serverMsg = new ServerMessage();
    		serverMsg.setType("test");
            return serverMsg;
    	}
    	
    	// invocation
		if(clientMsg.getType().equals("invocation")) {
			ServerMessage serverMsg = new ServerMessage();
//			System.out.println(clientMsg.getType());
//			System.out.println(clientMsg.getBody());

			RefactoringManager manager = new RefactoringManager();
//			TestRefactoringManager testManager = new TestRefactoringManager();
			String jsonResponse = null;
//			jsonResponse = testManager.handleInvocation(clientMsg.getBody());
			jsonResponse = manager.handleInvocation(clientMsg.getBody());
//			Gson gson = new Gson();
//			SimpleMessage result = gson.fromJson(jsonResponse, SimpleMessage.class);
			
			serverMsg.setType("transformation");
			serverMsg.setBody(jsonResponse);
			
			long end_time = System.nanoTime();
	    	double difference = (end_time - start_time) / 1e6;
			serverMsg.setMetadata(difference+"");	//ms
//			System.out.println(jsonResponse.toString());
			return serverMsg;
		}
		
		// server response
		ServerMessage serverMsg = new ServerMessage();
		serverMsg.setType("Error.. unknown type");
		
        return serverMsg;
    }

}
