package q4blocks.service.ws;

//import com.google.gson.Gson;

public class ServerMessage {

    private String type;
    private String body;
    private String metadata;

    public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getBody() {
		return body;
	}
	public void setBody(String jsonResponse) {
		this.body = jsonResponse;
	}
	
	public String getMetadata() {
		return metadata;
	}
	public void setMetadata(String metadata) {
		this.metadata = metadata;
	}
	@Override
	public String toString() {
		return "ServerMessage [" + (type != null ? "type=" + type + ", " : "") + (body != null ? "body=" + body : "")
				+ "]";
	}
	
//	public static void main(String[] args) {
//		TestRefactoringManager manager = new TestRefactoringManager();
//		String jsonResponse = manager.handleInvocation("");
//		Gson gson = new Gson();
//		ServerMessage serverMsg = gson.fromJson(jsonResponse, ServerMessage.class);
//		System.out.println(serverMsg);
//	}
}
