package q4blocks.service.ws;

import java.util.Map;

public class Invocation {
	private String refactoring;
	private String blockId;
	private String blocks;
	private Map<String, String> targets;
	

	public Map<String, String> getTargets() {
		return targets;
	}

	public void setTargets(Map<String, String> targets) {
		this.targets = targets;
	}

	public String getBlockId() {
		return blockId;
	}

	public String getBlocks() {
		return blocks;
	}

	public String getRefactoring() {
		return refactoring;
	}

	public void setBlockId(String blockId) {
		this.blockId = blockId;
	}

	public void setBlocks(String blocks) {
		this.blocks = blocks;
	}

	public void setRefactoring(String refactoring) {
		this.refactoring = refactoring;
	}

	@Override
	public String toString() {
		return "Invocation [refactoring=" + refactoring + ", blockId=" + blockId + ", blocks=" + blocks + "]";
	}
	
}
